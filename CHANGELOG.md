## [1.2.1](https://gitlab.com/gitm8/npmpty/compare/v1.2.0...v1.2.1) (2018-06-10)


### Bug Fixes

* reactivate npm publish ([2f7c484](https://gitlab.com/gitm8/npmpty/commit/2f7c484))

# [1.2.0](https://gitlab.com/gitm8/npmpty/compare/v1.1.1...v1.2.0) (2018-06-09)


### Bug Fixes

* change bin name ([3c1bbae](https://gitlab.com/gitm8/npmpty/commit/3c1bbae))
* set root template path ([28e4cec](https://gitlab.com/gitm8/npmpty/commit/28e4cec))


### Features

* add old npmpty with templates ([8c97e55](https://gitlab.com/gitm8/npmpty/commit/8c97e55))

<a name="1.1.1"></a>
## [1.1.1](https://gitlab.com/gitm8/npmpty/compare/v1.1.0...v1.1.1) (2018-06-06)


### Bug Fixes

* update package name ([5668873](https://gitlab.com/gitm8/npmpty/commit/5668873))
* use existing package name ([96e1662](https://gitlab.com/gitm8/npmpty/commit/96e1662))

<a name="1.1.0"></a>
# [1.1.0](https://gitlab.com/gitm8/npmpty/compare/v1.0.0...v1.1.0) (2018-06-06)


### Features

* add another nonsense function to test release ([79623fa](https://gitlab.com/gitm8/npmpty/commit/79623fa))

<a name="1.0.0"></a>
# 1.0.0 (2018-06-05)


### Features

* add bunch of configuration & test files ([2c499cd](https://gitlab.com/gitm8/npmpty/commit/2c499cd))
* intialize empty node project ([da78540](https://gitlab.com/gitm8/npmpty/commit/da78540))
