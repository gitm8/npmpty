#!/usr/bin/env node
const yeoman = require('yeoman-environment');
const initGenerator = require('./initGenerator');

const env = yeoman.createEnv();

env.registerStub(initGenerator, 'init:app');

env.run(process.argv[2], (err) => {
  if (err) throw err;
});
