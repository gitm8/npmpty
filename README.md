![npmpty logo](http://www.antlike.tech/assets/images/npmpty_logo_small.png)

[Contributing](./CONTRIBUTING.md) | [License](./LICENSE.md) | [Code of Conduct](./docs/CODE_OF_CONDUCT.md)

[![version](https://img.shields.io/npm/v/npmpty.svg)](https://www.npmjs.com/package/npmpty)
[![npm downloads](https://img.shields.io/npm/dt/npmpty.svg)](https://www.npmjs.com/package/npmpty)
[![liscense](https://img.shields.io/npm/l/npmpty.svg)](https://www.npmjs.com/package/npmpty)
[![Known Vulnerabilities](https://snyk.io/test/github/thegitm8/npmpty/badge.svg)](https://snyk.io/test/github/thegitm8/npmpty)
[![pipeline status](https://gitlab.com/gitm8/mpt/badges/master/pipeline.svg)](https://gitlab.com/gitm8/npmpty/commits/master)
[![coverage](https://gitlab.com/gitlab-org/gitlab-ce/badges/master/coverage.svg?job=coverage)](#)
[![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release)
[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/)

`npmpty` is a node.js module bootstrapper.

<!-- TOC -->

- [Features](#features)
- [Installation](#installation)
- [Usage](#usage)

<!-- /TOC -->

## Features

* linting
* Continious Integration (CI)
* automatic releases
* checks if dependencies
  * have known vulnerabilities
  * are outdated
* transpilation


## Installation
```sh
npm i -g npmpty
```

`npmpty` depends on the `git` commandline tool installed on the host system.


## Usage
```sh
mpt init
```
